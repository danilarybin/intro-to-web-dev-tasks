import {createHeader} from './header/header';
import {createMain} from './main/main';
import {createFooter} from './footer/footer';
const body = document.querySelector('body');
const { header, discovera, joina, signupa } = createHeader();
const main = createMain(discovera, joina, signupa);
const footer = createFooter();
  
body.appendChild(header);
body.appendChild(main);
body.appendChild(footer);